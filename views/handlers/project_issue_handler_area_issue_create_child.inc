<?php

/**
 * @file
 * Definition of project_issue_handler_area_issue_create_child.
 */

/**
 * Views area text handler for creating a new child issue.
 */
class project_issue_handler_area_issue_create_child extends views_handler_area {

  public function render($empty = FALSE) {
    // Get the ID of the current issue (and prospective parent) from the
    // argument.
    $parent_nid = $this->view->argument['nid']->get_value();

    if (
      ($parent_project = node_load(node_load($parent_nid)->field_project[LANGUAGE_NONE][0]['target_id'])) &&
      (_project_is_issue_tracker_enabled($parent_project))
    ) {
      return l(
        t('Add child issue'),
        'node/add/project-issue/' . $parent_project->field_project_machine_name[LANGUAGE_NONE][0]['value'],
        [
          'query' => [
            'field_issue_parent' => $parent_nid,
          ],
        ]
      );
    }
  }

}
