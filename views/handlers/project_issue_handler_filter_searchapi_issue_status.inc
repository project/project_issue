<?php

class SearchApiViewsHandlerFilterIssueStatus extends SearchApiViewsHandlerFilterOptions {

  protected function get_value_options() {
    parent::get_value_options();
    $this->value_options = [
      'Open' => t('- Open issues -'),
      'Closed' => t('- Closed issues -'),
    ] + $this->value_options;
  }

  public function query() {
    if (isset($this->value['Open'])) {
      unset($this->value['Open']);
      $this->value += drupal_map_assoc(project_issue_open_states());
    }
    elseif (($key = array_search('Open', $this->value, TRUE)) !== FALSE) {
      unset($this->value[$key]);
      $this->value += drupal_map_assoc(project_issue_open_states());
    }
    if (isset($this->value['Closed'])) {
      unset($this->value['Closed']);
      $this->value += drupal_map_assoc(project_issue_closed_states());
    }
    elseif (($key = array_search('Closed', $this->value, TRUE)) !== FALSE) {
      unset($this->value[$key]);
      $this->value += drupal_map_assoc(project_issue_closed_states());
    }
    parent::query();
  }

}
