<?php

/**
 * Filter issues based on their version.
 *
 * This filter only returns values to select if it's exposed and the view has
 * a project argument (either nid or uri), and that argument is first.
 */
class ProjectIssueHandlerFilterSearchApiVersion extends SearchApiViewsHandlerFilterOptions {
  private $api_version_strings = [];

  /**
   * {@inheritdoc}
   */
  public function has_extra_options() {
    return TRUE;
  }

  public function option_definition() {
    $options = parent::option_definition();

    $options['project_argument'] = ['default' => NULL];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function extra_options_form(&$form, &$form_state) {
    // Figure out what arguments this view has defined.
    $arguments[0] = t('<None>');
    $view_args = $this->view->display_handler->get_handlers('argument');
    if (!empty($view_args)) {
      foreach ($view_args as $id => $argument) {
        $arguments[$id] = $argument->ui_name();
      }
    }

    $form['project_argument'] = [
      '#type' => 'select',
      '#title' => t('Project argument used to determine options.'),
      '#options' => $arguments,
      '#default_value' => $this->options['project_argument'],
    ];
  }

  protected function get_value_options() {
    $this->value_options = [];

    if (!empty($this->view->args)) {
      $has_api_tid_regex = project_release_has_api_tid_regex();
      $project_nid = $this->view->argument[$this->options['project_argument']]->get_value();
      $this->value_options = project_release_get_available_versions($project_nid);
      $wildcard_options = [];
      $series_options = [];
      foreach ($this->value_options as $key => &$series) {
        // Match a version number prefix. Either the API compatibility prefix,
        // if it exists; or the first verion number component, likely the major
        // version number.
        if (preg_match($has_api_tid_regex, $key, $match)) {
          $wildcard_options['any_' . $match['api'] . '-'] = t('- Any @string -', ['@string' => $match['api'] . '-*']);
        }
        elseif (preg_match('/^[0-9]*\./', $key, $match)) {
          $wildcard_options['any_' . $match[0]] = t('- Any @string -', ['@string' => $match[0] . '*']);
        }
        // Match a release “branch” series.
        $series_options['all_' . $key] = t('All @series', ['@series' => $key]);
      }
      $this->value_options = $wildcard_options + $series_options + $this->value_options;

      // Redirect legacy API term options.
      if (!empty($this->options['expose']['identifier']) && menu_get_item()['path'] !== 'project/autocomplete/issues/count') {
        $identifier = $this->options['expose']['identifier'];
        if (($input = $this->view->get_exposed_input()) && !empty($input[$identifier])) {
          $options = options_array_flatten($this->value_options);
          if (is_array($input[$identifier])) {
            $redirect_query = [];
            foreach ($input[$identifier] as $key => $value) {
              if (!isset($options[$value]) && preg_match($has_api_tid_regex, $value . '-')) {
                $redirect_query[$key] = 'any_' . $value . '-';
              }
            }
            if (!empty($redirect_query)) {
              $query = drupal_get_query_parameters();
              $query[$identifier] = $redirect_query + $query[$identifier];
              drupal_goto(current_path(), ['query' => $query]);
            }
          }
          else {
            if (!isset($options[$input[$identifier]]) && preg_match($has_api_tid_regex, $input[$identifier] . '-')) {
              drupal_goto(current_path(), [
                'query' => [$identifier => 'any_' . $input[$identifier] . '-'] + drupal_get_query_parameters(),
              ]);
            }
          }
        }
      }
    }
  }

  public function query() {
    // Expand “Any/All series” versions.
    foreach ($this->value as &$value) {
      if (preg_match('/^(?<type>any|all)_(?<series>.*)$/', $value, $match)) {
        if ($match['type'] === 'any') {
          // Use option groups which match the version number prefix and
          // existence of an API prefix.
          if (is_null($this->value_options)) {
            $this->get_value_options();
          }
          $has_api_tid_regex = project_release_has_api_tid_regex();
          $has_api_tid = preg_match($has_api_tid_regex, $match['series']);
          foreach (array_filter($this->value_options, 'is_array') as $key => $options) {
            if (strpos($key, $match['series']) === 0 && preg_match($has_api_tid_regex, $key) === $has_api_tid) {
              $this->value += $options;
            }
          }
        }
        else {
          // Match type is all, use the options from the option group.
          $this->value += $this->value_options[$match['series']];
        }
      }
    }
    return parent::query();
  }

}
