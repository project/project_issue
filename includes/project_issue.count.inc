<?php

/**
 * @file
 * Contains functionality for the issue count input filter callback.
 */

/**
 * Page callback for project issue counts.
 *
 * @see js/project-issue-count.js
 */
function project_issue_count_json() {
  $contents = json_decode(file_get_contents('php://input'), TRUE);
  if (empty($contents['items'])) {
    drupal_json_output(array(
      'counts' => array(),
    ));
    return;
  }
  $user_provided_items = $contents['items'];
  $filtered_items = array_filter($contents['items'], 'is_string');
  if (count($user_provided_items) !== count($filtered_items)) {
    // There was invalid data here, bail out early.
    drupal_json_output(array(
      'counts' => array(),
    ));
    return;
  }
  $counts = array();
  $seen = array();
  foreach ($filtered_items as $item) {
    // Remove host and scheme, so we have just path and query.
    $parts = parse_url($item);
    $parts += array('path' => '', 'query' => '');
    $matches = array();
    if (!preg_match('@/project/issues/search/(?<machine_name>[a-z0-9_]+)$@', trim($parts['path']), $matches)) {
      // Invalid href format.
      continue;
    }
    if (!($project = project_load($matches['machine_name'])) || $project->status == 0) {
      // No such project or unpublished.
      continue;
    }

    // Validate the parameters of the URL.
    $parameters = array();
    parse_str($parts['query'], $parameters);
    $parameters = array_filter($parameters, function ($value, $key) {
      if (empty($value)) {
        return FALSE;
      }
      $valid_fields = array(
        'participants' => 'string',
        'text' => 'string',
        'submitted' => 'string',
        'assigned' => 'string',
        'status' => 'numbers',
        'priorities' => 'numbers',
        'categories' => 'numbers',
        'component' => 'strings',
        'version' => 'strings',
        'project_issue_followers' => 'string',
        'issue_tags' => 'string',
        'issue_tags_op' => 'string',
      );
      if (!array_key_exists($key, $valid_fields)) {
        return FALSE;
      }
      switch ($valid_fields[$key]) {
        case 'string':
          return is_string($value);

        case 'numbers':
          // These are either numbers or 'Open'.
          return $value == is_array($value) && array_filter($value, 'is_scalar');

        case 'strings':
          return $value == is_array($value) && array_filter($value, 'is_string');

        default:
          return FALSE;
      }
    }, ARRAY_FILTER_USE_BOTH);

    $truncated = $parts['path'] . '?' . drupal_http_build_query($parameters);

    if (isset($seen[$truncated])) {
      // We've already calculated a count for this URL.
      $counts[$item] = array(
        'item' => $item,
        'count' => $seen[$truncated],
      );
      continue;
    }
    $cid = $project->nid . ':' . hash('sha256', $truncated);
    if (($cached = cache_get($cid, 'cache_issue_search_counts')) && $cached->data) {
      // Cache-hit.
      $counts[$item] = array(
        'item' => $item,
        'count' => $cached->data,
      );
      $seen[$truncated] = $cached->data;
      continue;
    }

    // Cache-miss.
    $display = 'page';
    if (!$view = views_get_view('project_issue_search_project_searchapi')) {
      $view = views_get_view('project_issue_search_project');
      $display = 'page_1';
    }
    // Reset any validation errors from previous views exposed forms.
    drupal_static_reset('form_set_error');
    $view->set_exposed_input($parameters);
    if (!$view || !$view->access($display)) {
      continue;
    }

    $view->get_total_rows = TRUE;
    $view->set_arguments([$project->nid]);
    $view->execute($display);
    $results = $view->total_rows;
    $seen[$truncated] = $results;
    cache_set($cid, $results, 'cache_issue_search_counts');
    $counts[$item] = array(
      'item' => $item,
      'count' => $results,
    );
  }
  // Flush the message buffer in case of broken links.
  drupal_get_messages();
  drupal_json_output(array(
    'counts' => array_values($counts),
  ));
}
