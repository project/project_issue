<?php

/**
 * Form builder for the search box in the issue cockpit block.
 */
function project_issue_issue_cockpit_searchbox($form, &$form_state, $node) {
  $machine_name = $node->field_project_machine_name[LANGUAGE_NONE][0]['value'];
  $search_path = "project/issues/$machine_name";
  $adv_search_path = "project/issues/search/$machine_name";
  return array(
    '#action' => url($search_path),
    '#method' => 'get',
    'text' => array(
      '#title' => t('Search'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#size' => 20,
    ),
    'status' => array(
      '#type' => 'hidden',
      '#value' => 'All',
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Search'),
      '#name' => '',
    ),
    'advanced' => array(
      '#markup' => l(t('Advanced search'), $adv_search_path),
    ),
  );
}

/**
 * Theme template preprocess to declare variables for the issue cockpit.
 */
function template_preprocess_project_issue_issue_cockpit(&$vars) {
  $node = $vars['node'];

  $machine_name = $node->field_project_machine_name[LANGUAGE_NONE][0]['value'];

  // Flags that indicate what kind of access to project issues to allow.
  $vars['view_issues'] = !empty($node->field_project_has_issue_queue[LANGUAGE_NONE][0]['value']);

  if ($vars['view_issues']) {
    $vars['path'] = "project/issues/$machine_name";
    $vars['form'] = drupal_get_form('project_issue_issue_cockpit_searchbox', $node);

    $query = db_select('node', 'n');
    $query->innerJoin('field_data_field_project', 'p', "n.nid = p.entity_id AND p.entity_type = 'node'");
    $query->innerJoin('field_data_field_issue_category', 'c', "n.nid = c.entity_id AND c.entity_type = 'node'");
    $query->innerJoin('field_data_field_issue_status', 's', "n.nid = s.entity_id AND s.entity_type = 'node'");
    $query->condition('n.status', 1);
    $query->condition('p.field_project_target_id', $node->nid);
    $query->addTag('node_access');

    $vars['categories'] = [];
    foreach (array_intersect_key(
        ['All' => t('All issues')] + project_issue_categories(),
        array_filter(variable_get('project_issue_cockpit_categories', ['All' => 'All', 1 => 'Bug reports']))
    ) as $category => $name) {
      $cquery = clone($query);
      if ($category !== 'All') {
        $cquery->condition('c.field_issue_category_value', $category);
      }

      $vars['categories'][$category] = [
        'name' => $name,
        'total' => $cquery->countQuery()->execute()->fetchField(),
        'extra' => [],
      ];

      $temp = clone($cquery);
      $temp->condition('s.field_issue_status_value', project_issue_open_states());
      $vars['categories'][$category]['open'] = $temp->countQuery()->execute()->fetchField();

      foreach (variable_get('project_issue_cockpit_extra_counts', []) as $label => $extra) {
        $temp = clone($cquery);
        foreach ($extra['conditions'] as $field => $value) {
          $temp->condition($field, $value);
        }
        $vars['categories'][$category]['extra'][t($label)] = [
          'count' => $temp->countQuery()->execute()->fetchField(),
          'query' => $extra['query'],
        ];
      }
    }

    $vars['issue_subscribe'] = l(t('Subscribe via e-mail'), "project/issues/subscribe-mail/$machine_name", array('query' => drupal_get_destination()));

    if (module_exists('sampler') && module_exists('d3_sparkline')) {
      $vars['issue_metrics'] = '';
      foreach (array(
        'project_issue_metrics_new_issues',
        'project_issue_metrics_response_rate',
        'project_issue_metrics_first_response',
        'project_issue_metrics_open_bugs',
        'project_issue_metrics_participants',
      ) as $view_name) {
        $view = views_get_view($view_name);
        if ($view && $view->access('default')) {
          $view->set_display('default');
          $view->pre_execute(array($node->nid));
          $view->execute();
          if (!empty($view->result)) {
            $vars['issue_metrics'] .= '<div class="metric-container">';
            $vars['issue_metrics'] .= '<span class="metric-label">' . $view->get_title() . '</span>';
            $vars['issue_metrics'] .= $view->display_handler->preview();
            $vars['issue_metrics'] .= '</div>';
          }
          $view->post_execute();
        }
      }
    }
  }
}
