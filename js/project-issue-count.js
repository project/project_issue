(function (Drupal, document) {
  'use strict';

  Drupal.behaviors.projectIssueCount = {
    attach: context => {
      // Replace jQuery with DOM object.
      if (context.jquery !== undefined) {
        context = context.get(0);
      }

      if (document.body.classList.contains('project-issue-count-processed')) {
        return;
      }
      document.body.classList.add('project-issue-count-processed')
      const items = Object.values(context.querySelectorAll('a:not(.no-issue-count)'))
        .filter(el => el.getAttribute('href')
          && el.getAttribute('href').match(/\/project\/issues\/search\/[a-z0-9_]+\?/)
          // Exclude pager and click-sort
          && !el.closest('ul.pager, th'))
        .map(el => el.getAttribute('href'));
      if (items.length === 0) {
        return;
      }
      var url = Drupal.settings.basePath + 'project/autocomplete/issues/count';
      fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({items}),
      })
        .then(response => response.json())
        .then(result => {
          result.counts.forEach(item => {
            const elements = context.querySelectorAll(`a[href="${item.item}"]`);
            if (elements) {
              Object.values(elements).forEach(el => {
                const issueCount = document.createElement('span');
                if (item.count == null) {
                  issueCount.textContent = ` [${Drupal.t('broken link')}]`;
                }
                else {
                  issueCount.textContent = ` [${Drupal.formatPlural(item.count, '1 issue', '@count issues')}]`;
                }
                issueCount.classList.add('issue-count');
                el.appendChild(issueCount);
              });
            }
          })
        })
        .catch(error => {
          console.error('Error:', error);
        });
    }
  };

})(Drupal, document);
