<?php print t('To avoid duplicates, please search before submitting a new issue.'); ?>

<?php if ($view_issues): ?>
  <?php print drupal_render($form); ?>
  <div class="issue-cockpit-categories">
    <?php foreach ($categories as $key => $category): ?>
      <div class="issue-cockpit-<?php print $key; ?>">
        <h3><?php print $categories[$key]['name']; ?></h3>
        <div class="issue-cockpit-totals">
          <?php print l(t('!open open', ['!open' => number_format($categories[$key]['open'])]), $path, ['query' => ['categories' => $key]]); ?>,
          <?php foreach ($category['extra'] as $label => $extra): ?>
            <?php if ($extra['count'] != 0): ?>
              <?php print l(t('!count !label', ['!count' => number_format($extra['count']), '!label' => $label]), $path, ['query' => ['categories' => $key] + $extra['query'], 'html' => TRUE]); ?>,
            <?php endif; ?>
          <?php endforeach; ?>
          <?php print l(t('!total total', ['!total' => number_format($categories[$key]['total'])]), $path, ['query' => ['status' => 'All', 'categories' => $key]]); ?>
        </div>
      </div>
    <?php endforeach; ?>
  </div>

  <div class="issue-cockpit-subscribe">
    <?php print $issue_subscribe; ?>
  </div>

  <?php if (isset($issue_metrics) && !empty($issue_metrics)): ?>
    <div class="issue-cockpit-metrics">
      <h3><?php print t('Statistics'); ?></h3>
      <?php print $issue_metrics; ?>
      <div><small>2 year graph, updates weekly</small></div>
    </div>
  <?php endif; ?>
<?php endif; ?>
